#!/bin/bash
# Classement fichier
OPEH=`file --mime-type -b $i | tr "/" " " | awk '{print $1}'`
mkdir -p Downloads
cd Downloads
mkdir -p Apps Musique Images Docs Vidéos Autres
if [ -f ~/M3206/tp3/*/classer.sh ];then;mv ~/M3206/tp3/*/classer.sh ~/M3206/tp3;fi
if [ -f * ];then
	for i in `ls`
		do{if [ $OPEH == "text" ];then;mv $i Docs/;else;if [ $OPEH == "application" ];then;mv $i Apps/;else;if [ $OPEH == "audio" ];then;mv $i Musique/;else;if [ $OPEH == "image" ];then;mv $i Images/;else;if [ $OPEH == "video" ];then;mv $i Vidéos/;else;mv $i Autres;fi}
		ls $i/
	done
fi
