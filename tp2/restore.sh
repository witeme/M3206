#!/bin/bash
# Restauration d'un archive

cd ~/M3206/tp2/backup
ARCHIVE=`ls -goth | grep \.tar.gz | head -1 | awk '{print $7}'`
DIRARCHVDAY=`ls -goth | stat -c %x *.tar.gz | head -1 | awk '{print $1}'`
DIRARCHVHOR=`ls -goth | grep \.tar.gz | head -1 | awk '{print $6}'`
#echo $ARCHIVE $DIRARCHVDAY $DIRARCHVHOR
cd ..
mkdir -p archives_dezip
cd archives_dezip
mkdir -p extracted_$DIRARCHVDAY-$DIRARCHVHOR
cd extracted_$DIRARCHVDAY-$DIRARCHVHOR
if [ -d root ]
then
	rm -fr root
	tar xvf ~/M3206/tp2/backup/$ARCHIVE
fi
cd ~/M3206/tp2
