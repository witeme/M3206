#!/bin/bash
#  Tentative de connexion à internet

echo "[...]Checking Internet connection [...]"
ping -c 4 www.google.com > /dev/null
TEST=$?

if [ "$TEST" = 0 ]
	then
		echo "[...] Internet access OK           [...]"
		exit 0
	else
		echo "[/!\] Not connected to Internet    [/!\]"
		echo "[/!\] Please check configuration   [/!\]"
fi
