#!/bin/bash
# Verifier lancement SSH

dpkg --list | grep ssh > /dev/null
if [ $? -ne 1 ]
	then
		echo "[...] ssh: installé        [...]"
		ps -e | grep ssh > /dev/null
		if [ $? -ne 1 ]
			then
				echo "[...] ssh: le service est déjà lancé [...]
			else
				echo "[/!\] ssh: service pas lancé[/!\] lancer commande /etc/init.d/sshd start"
		fi
	else
		echo "[...] ssh: non installé    [...]"
fi
