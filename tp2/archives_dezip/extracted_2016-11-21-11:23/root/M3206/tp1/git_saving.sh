#!/bin/bash
# Enregistrement GIT

git status
echo "Enregistrer quel fichier?"
read savefile
echo "Exportation du fichier $savefile en cours ..."
echo "-       -"
echo "-   -   -"
echo " - - - - "
echo "  - - -  "
echo ""
echo "----------------"
git add $savefile > /dev/null
git commit -m "$savefile enregistré" > /dev/null
git push origin master
echo "$savefile est bien exporté."
