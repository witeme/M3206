#!/bin/bash
# Mise-à-jour système

if [ $(whoami) = "root" ]
	then
		echo "[...] update system  [...]"
		apt-get update > /dev/null
		echo "[...] upgrade system [...]"
		apt-get upgrade > /dev/null
	else
		echo "[...] Mets-toi en root! [...]"
fi
