#!/bin/bash
# Verifier si paquets installés

echo "Verifier quels paquets?"
read verifile

dpkg --list | grep $verifile > /dev/null
if [ $? -ne 1 ]
	then
		echo "[...] $verifile: installé       [...]"
	else
		echo "[/!\] $verifile: pas installé   [/!\] lancer la commande: 'apt-get install $verifile'"
fi
