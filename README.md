## TD FIND -- Wesley ITEME -- 22/11/2016 -- From M. RAZAFINDRALAMBO Tahiry

__Fichier readme à manipuler très précautionneusement. (Juste pour rire)__

##### option `-exec` de `find`

L'option `-exec` est une option à utiliser lorsqu'on est sûr de vouloir executer telle commande sans risque de commetre une erreur fatale.

##### option `-ok` de `find`

L'option `-ok`, quant à elle, permet d'éviter une précipitation de ses actes: en clair, si l'on veut faire telle chose en ajoutant `-ok` à `find`, il nous demande de confirmer l'action à effectuer.

La commande `cpio` sert à copier les dossiers ou fichiers, mais tout en laissant les permissions aux fichiers copiés sur les copies de fichiers.

```bash
# find <repertoire> -name <type fichier> -ok cp {} <repertoire destination> \;
```
